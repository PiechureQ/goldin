import sys


class Error(object):
    def __init__(self, tokens):
        self.tokens = tokens

    def error(self, token):
        sys.exit('syntax_error - {token} at line {line}'.format(token=token.to_string(), line=self._count_line(token)))

    def _count_line(self, token):
        indexes_of_EOL = []
        index_of_token = self.tokens.index(token)

        for t in self.tokens:
            if t.type == 'EOL':
                indexes_of_EOL.append(self.tokens.index(t))

        for i in indexes_of_EOL:
            line = indexes_of_EOL.index(i)
            if i >= indexes_of_EOL[-1]:
                return line + 2
            if i < index_of_token < indexes_of_EOL[line + 1]:
                return line + 2

        raise Exception('could not locate error in source file')