from sourceparser import lexer, Parser, Block, Assignment, Variable, ConstantInt, Stmt, BinOp, If, Print, Loop, Comparison, \
    parse

import unittest

file = open('E:\projekty\Python\goldin\source.gld')
source_prototype = file.read()


class TestLexerMethods(unittest.TestCase):

    def test_test(self):
        for token in lexer.lex(source_prototype):
            print(token)


class TestParser(unittest.TestCase):
    def test_get_next(self):
        # given
        lex = Lexer('a = 1')
        parser = Parser(lex.tokens)
        parser.token = parser.tokens[1]
        # when
        token = parser.get_next_token(parser.token)
        # then
        assert token.to_string() == parser.tokens[2].to_string()

    def test_get_recent(self):
        # given
        lex = Lexer('a = 1')
        parser = Parser(lex.tokens)
        parser.token = parser.tokens[1]
        # when
        token = parser.get_recent_token(parser.token)
        # then
        assert token.to_string() == parser.tokens[0].to_string()

    def test_parse(self):
        # given
        source = 'a'
        source1 = '1 + 1'
        source2 = '1'
        # when
        ast = parse(source)
        ast1 = parse(source1)
        ast2 = parse(source2)
        # then
        assert ast == Block([Stmt(Variable('a'))])
        assert ast1 == Block([Stmt(BinOp("+", ConstantInt(1), ConstantInt(1)))])
        assert ast2 == Block([Stmt(ConstantInt(1))])

    def test_assignment(self):
        # given
        source = 'a = 1'
        # when
        ast = parse(source)
        # then
        assert ast == Block([Assignment('a', ConstantInt(1))])

    def test_parse_multiline(self):
        # given
        source = '''
        a = 2
        a
        12  
        '''
        # when
        ast = parse(source)
        # then
        assert ast == Block([Assignment('a', ConstantInt(2)),
                             Stmt(Variable('a')),
                             Stmt(ConstantInt(12))])

    def test_parse_binary_operations(self):
        # given
        source = '''
        2 - 1
        1 +2
        2 * 2
        4/2
        '''
        # when
        ast = parse(source)
        # then
        assert ast == Block([Stmt(BinOp("-", ConstantInt(2), ConstantInt(1))),
                             Stmt(BinOp("+", ConstantInt(1), ConstantInt(2))),
                             Stmt(BinOp("*", ConstantInt(2), ConstantInt(2))),
                             Stmt(BinOp("/", ConstantInt(4), ConstantInt(2)))])

    def test_parse_comparison(self):
        # given
        source = '''
        if 1 < 2{}
        if 2 > 1{}
        if 2 <= 2{}
        if 1 >= 1{}
        if 1 != 2{}
        if 1 == 1{}'''
        # when
        ast = parse(source)
        # then
        assert ast == Block([If(Comparison('<', ConstantInt(1), ConstantInt(2)), Block([])),
                             If(Comparison('>', ConstantInt(2), ConstantInt(1)), Block([])),
                             If(Comparison('<=', ConstantInt(2), ConstantInt(2)), Block([])),
                             If(Comparison('>=', ConstantInt(1), ConstantInt(1)), Block([])),
                             If(Comparison('!=', ConstantInt(1), ConstantInt(2)), Block([])),
                             If(Comparison('==', ConstantInt(1), ConstantInt(1)), Block([]))])

    def test_variable_operation(self):
        # given
        source = '''
        a = 2
        a - 1
        '''
        # when
        ast = parse(source)
        # then
        assert ast == Block([Assignment('a', ConstantInt(2)),
                             Stmt(BinOp('-', Variable('a'), ConstantInt(1)))])

    def test_if(self):
        # given
        source = '''if 1{
                    1 +2
                    }'''
        # when
        ast = parse(source)
        # then
        assert ast == Block([If(ConstantInt(1),
                                Block([Stmt(BinOp("+", ConstantInt(1), ConstantInt(2)))]))])

    def test_print(self):
        # given
        source = '''
                print 1
                print a'''
        # when
        ast = parse(source)
        # then
        assert ast == Block([Print(ConstantInt(1)),
                             Print(Variable('a'))])

    def test_loop(self):
        # given
        source = '''a = 2
                    loop a == 2{
                    a = 3}'''
        # when
        ast = parse(source)
        # then
        assert ast == Block([Assignment('a', ConstantInt(2)),
                             Loop(Comparison('==', Variable('a'), ConstantInt(2)),
                                  Block([Assignment('a', ConstantInt(3))]))])

    def test_scope(self):
        # given
        source = '''a = 2
                    if a == 2{
                    print a
                    a = 3}
                    print a'''
        # when
        ast = parse(source)
        # then
        assert ast == Block([Assignment('a', ConstantInt(2)),
                             If(Comparison('==', Variable('a'), ConstantInt(2)),
                                Block([Print(Variable('a')), Assignment('a', ConstantInt(3))])),
                             Print(Variable('a'))])
