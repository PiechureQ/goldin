import unittest

from interpreter import interpret

file = open('E:\projekty\Python\goldin\source.gld')
source_prototype = file.read()


class TestInterpreter(unittest.TestCase):
    def test_prototype(self):
        interpret(source_prototype)

    def test_interpret(self):
        # given
        source = '''
        2 + 3
        '''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 5

    def test_LOAD_CONSTANT(self):
        # given
        source = '1'
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.constants[0].intval == 1

    def test_BINARY_ADD(self):
        # given
        source = '1 + 1'
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 2

    def test_BINARY_SUB(self):
        # given
        source = '2 - 1'
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 1

    def test_BINARY_MUL(self):
        # given
        source = '2* 2'
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 4

    def test_BINARY_DIV(self):
        # given
        source = '4 /2'
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 2

    def test_comparison_operations(self):
        # given
        source = '''
        if 1 < 2{print 1}
        if 2 > 3{print 2}'''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.check_stack[0].intval == True
        assert interpreter.check_stack[1].intval == 1
        assert interpreter.check_stack[2].intval == False

    def test_variable_operation(self):
        # given
        source = '''
        a = 1
        a + 2
        '''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 3

    def test_if(self):
        # given
        source = '''if 1{
                    1 +2
                    }'''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.result.intval == 3

    def test_print(self):
        # given
        source = '''a = 3
                    print 1
                    print a'''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.check_stack[0].intval == 1
        assert interpreter.check_stack[1].intval == 3

    def test_loop(self):
        # given
        source = '''a = 2
                    loop a == 2{
                    a = 3}'''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.check_stack[0].is_true()
        assert not interpreter.check_stack[1].is_true()

    def test_scope(self):
        # given
        source = '''a = 2
                    loop a == 2{
                    print a
                    a = 3}
                    print a'''
        # when
        interpreter = interpret(source)
        # then
        assert interpreter.check_stack[0].is_true()
        assert interpreter.check_stack[1].intval == 2
        assert not interpreter.check_stack[2].is_true()
        assert interpreter.check_stack[3].intval == 3
