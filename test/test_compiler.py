from bytecode import compile_ast
from sourceparser import parse

import unittest


class TestCompilator(unittest.TestCase):
    def test_number(self):
        # given
        source = '1'
        expected = '''
        LOAD_CONSTANT 0
        DISCARD_TOP 0
        '''
        # when
        bc = compile_ast(parse(source))
        # then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()

    def test_add(self):
        # given
        source = '1 + 1'
        expected = '''
        LOAD_CONSTANT 0
        LOAD_CONSTANT 1
        BINARY_ADD 0
        DISCARD_TOP 0
        '''
        # when
        bc = compile_ast(parse(source))
        # then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()

    def test_binaryop(self):
        # given
        source = '''
        1 + 1
        2 - 1
        2 * 2
        4 / 2
        '''
        expected = '''
        LOAD_CONSTANT 0
        LOAD_CONSTANT 1
        BINARY_ADD 0
        DISCARD_TOP 0
        LOAD_CONSTANT 2
        LOAD_CONSTANT 3
        BINARY_SUB 0
        DISCARD_TOP 0
        LOAD_CONSTANT 4
        LOAD_CONSTANT 5
        BINARY_MUL 0
        DISCARD_TOP 0
        LOAD_CONSTANT 6
        LOAD_CONSTANT 7
        BINARY_DIV 0
        DISCARD_TOP 0
        '''
        # when
        bc = compile_ast(parse(source))
        # then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()

    def test_variable_operation(self):
        # given
        source = """
        a = 1
        a + 2
        """
        expected = '''
        LOAD_CONSTANT 0
        ASSIGN 0
        LOAD_VAR 0
        LOAD_CONSTANT 1
        BINARY_ADD 0
        DISCARD_TOP 0'''
        # when
        bc = compile_ast(parse(source))
        # then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()

    def test_if(self):
        # given
        source = '''if 1 >= 1{
                    1 +2
                    }'''
        expected = '''
        LOAD_CONSTANT 0
        LOAD_CONSTANT 1
        GREATER_EQUAL 0
        JUMP_IF_FALSE 16
        LOAD_CONSTANT 2
        LOAD_CONSTANT 3
        BINARY_ADD 0
        DISCARD_TOP 0 
        '''
        # when
        bc = compile_ast(parse(source))
        # then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()

    def test_print(self):
        #given
        source = '''print 1
                    print a'''
        expected = '''
        LOAD_CONSTANT 0
        PRINT 0
        LOAD_VAR 0
        PRINT 0'''
        #when
        bc = compile_ast(parse(source))
        #then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()

    def test_loop(self):
        #given
        source = '''a = 2
                    loop a < 3{
                    a = 3}'''
        expected = '''
        LOAD_CONSTANT 0
        ASSIGN 0
        LOAD_VAR 0
        LOAD_CONSTANT 1
        LESSER 0
        JUMP_IF_FALSE 18
        LOAD_CONSTANT 2
        ASSIGN 0
        JUMP_BACKWARD 4
        '''
        #when
        bc = compile_ast(parse(source))
        #then
        assert [i.strip() for i in expected.splitlines() if i.strip()] == bc.dump().splitlines()