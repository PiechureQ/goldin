import unittest

from error import Error
from interpreter import interpret
from sourceparser import Lexer

source = '''a = 1
        b=2
        if a == 1 {
            print b
        
        print a'''


class TestSyntaxError(unittest.TestCase):
    def test_count_line(self):
        # given
        lex = Lexer(source)
        err = Error(lex.tokens)
        # when
        line = err._count_line(lex.tokens[15])
        # then
        assert line == 4

    def test_variable_undefined(self):
        source = ''' 
                print a
                '''

        interpret(source)