import sys

from interpreter import interpret


def main(argv):
    if not len(argv) == 2:
        print(__doc__)
        return
    f = open(argv[1])
    source = f.read()
    f.close()

    interpret(source)
    return


if __name__ == '__main__':
    main(sys.argv)
