from bytecode import compile_ast
from sourceparser import parse


class G_Object(object):
    pass


class G_Int(G_Object):
    def __init__(self, intval):
        assert isinstance(intval, int)
        self.intval = intval

    def add(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval + other.intval)

    def sub(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval - other.intval)

    def mul(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval * other.intval)

    def div(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval // other.intval)

    def eq(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval == other.intval)

    def ne(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval != other.intval)

    def lt(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval < other.intval)

    def gt(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval > other.intval)

    def le(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval <= other.intval)

    def ge(self, other):
        if not isinstance(other, G_Int):
            raise Exception('Wrong type')
        return G_Int(self.intval >= other.intval)

    def is_true(self):
        return self.intval != 0

    def str(self):
        return str(self.intval)


class Interpreter(object):
    def __init__(self, bytecode):
        self.variables = [None] * bytecode.numvars
        self.bytecode = bytecode.dump().split()
        self.constants = bytecode.constants
        self.stack = []
        self.check_stack = []
        self.stack_pos = 0
        self.result = 0

    def interpret(self):
        while self.stack_pos < len(self.bytecode):
            opname = self.bytecode[self.stack_pos]
            self.stack_pos = getattr(self, opname)(self.stack_pos)

    def push(self, arg):
        self.stack.append(arg)

    def pop(self):
        return self.stack.pop()

    def LOAD_CONSTANT(self, pc):
        arg = int(self.bytecode[pc + 1])
        self.push(self.constants[arg])
        return pc + 2

    def LOAD_VAR(self, pc):
        arg = int(self.bytecode[pc + 1])
        self.push(self.variables[arg])
        return pc + 2

    def ASSIGN(self, pc):
        arg = int(self.bytecode[pc + 1])
        self.variables[arg] = self.pop()
        return pc + 2

    def BINARY_ADD(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.add(right))
        return pc + 2

    def BINARY_SUB(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.sub(right))
        return pc + 2

    def BINARY_MUL(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.mul(right))
        return pc + 2

    def BINARY_DIV(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.div(right))
        return pc + 2

    def EQUAL(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.eq(right))
        self.check_stack.append(left.eq(right))
        return pc + 2

    def GREATER_EQUAL(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.ge(right))
        self.check_stack.append(left.ge(right))
        return pc + 2

    def LESSER_EQUAL(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.le(right))
        self.check_stack.append(left.le(right))
        return pc + 2

    def NOT_EQUAL(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.ne(right))
        self.check_stack.append(left.ne(right))
        return pc + 2

    def LESSER(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.lt(right))
        self.check_stack.append(left.lt(right))
        return pc + 2

    def GREATER(self, pc):
        right = self.pop()
        left = self.pop()
        self.push(left.gt(right))
        self.check_stack.append(left.gt(right))
        return pc + 2

    def JUMP_IF_FALSE(self, pc):
        if not self.pop().is_true():
            pc = int(self.bytecode[pc + 1])
            return pc
        else:
            return pc + 2

    def DISCARD_TOP(self, pc):
        self.result = self.pop()
        return pc + 2

    def PRINT(self, pc):
        item = self.pop()
        print(item.str())
        self.check_stack.append(item)
        return pc + 2

    def JUMP_BACKWARD(self, pc):
        pc = int(self.bytecode[pc + 1])
        return pc


def interpret(source):
    bc = compile_ast(parse(source))
    interpreter = Interpreter(bc)
    interpreter.interpret()
    return interpreter  # for tests
