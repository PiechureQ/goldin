from error import Error
from rply import LexerGenerator

import bytecode

lg = LexerGenerator()

lg.ignore(r'\s+')
lg.add('IF', r'if')
lg.add('ELSE', r'else')
lg.add('LOOP', r'loop')
lg.add('PRINT', r'print')
lg.add('LBRACE', r'\{')
lg.add('RBRACE', r'\}')
lg.add('EQUAL', r'\=')

lg.add('ADD', '\+')
lg.add('SUB', '\-')
lg.add('MUL', '\*')
lg.add('DIV', '\/')

lg.add('GREATER', r'>')
lg.add('GREATER_EQUAL', r'\>=')
lg.add('SMALLER', r'<')
lg.add('SMALLER_EQUAL', r'<=')
lg.add('NOT_EQUAL', r'!=')

lg.add('NUMBER', r'\d+')
lg.add('VARIABLE', r'[a-zA-z_][a-zA-Z0-9_]*')

lexer = lg.build()


def create_parser(self):
    return Parser(self.tokens)


class Node:
    "abstract AST node"

    def __eq__(self, other):
        return (self.__class__ == other.__class__ and
                self.__dict__ == other.__dict__)

    def __ne__(self, other):
        return not self == other


class Block(Node):
    "list of statements"

    def __init__(self, stmts):
        self.stmts = stmts

    def compile(self, ctx):
        for stmt in self.stmts:
            stmt.compile(ctx)


class Stmt(Node):
    "a single statement"

    def __init__(self, expr):
        self.expr = expr

    def compile(self, ctx):
        self.expr.compile(ctx)
        ctx.emit(bytecode.DISCARD_TOP)


class Assignment(Node):
    "Assignment operation"

    def __init__(self, varname, expr):
        self.varname = varname
        self.expr = expr

    def compile(self, ctx):
        self.expr.compile(ctx)
        ctx.emit(bytecode.ASSIGN, ctx.register_var(self.varname))


class Comparison(Node):
    "Compare operation"

    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def compile(self, ctx):
        self.left.compile(ctx)
        self.right.compile(ctx)
        opname = {
            "==": bytecode.EQUAL,
            ">=": bytecode.GREATER_EQUAL,
            ">": bytecode.GREATER,
            "<=": bytecode.LESSER_EQUAL,
            "<": bytecode.LESSER,
            "!=": bytecode.NOT_EQUAL
        }
        ctx.emit(opname[self.op])


class Variable(Node):
    "reference to a variable"

    def __init__(self, varname):
        self.varname = varname

    def compile(self, ctx):
        ctx.emit(bytecode.LOAD_VAR, ctx.register_var(self.varname))


class ConstantInt(Node):
    "represent value"

    def __init__(self, intval):
        self.intval = intval

    def compile(self, ctx):
        from interpreter import G_Int
        g = G_Int(self.intval)
        ctx.emit(bytecode.LOAD_CONSTANT, ctx.register_constant(g))


class BinOp(Node):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def compile(self, ctx):
        self.left.compile(ctx)
        self.right.compile(ctx)
        opname = {
            "+": bytecode.BINARY_ADD,
            "-": bytecode.BINARY_SUB,
            "*": bytecode.BINARY_MUL,
            "/": bytecode.BINARY_DIV
        }
        ctx.emit(opname[self.op])


class If(Node):
    """ A very simple if"""

    def __init__(self, cond, body):
        self.cond = cond
        self.body = body

    def compile(self, ctx):
        self.cond.compile(ctx)
        ctx.emit(bytecode.JUMP_IF_FALSE, 0)
        jmp_pos = len(ctx.data) - 1
        self.body.compile(ctx)
        ctx.data[jmp_pos] = chr(len(ctx.data))


class Print(Node):
    def __init__(self, expr):
        self.expr = expr

    def compile(self, ctx):
        self.expr.compile(ctx)
        ctx.emit(bytecode.PRINT, 0)


class Loop(Node):
    """ Simple loop
    """

    def __init__(self, cond, body):
        self.cond = cond
        self.body = body

    def compile(self, ctx):
        pos = len(ctx.data)
        self.cond.compile(ctx)
        ctx.emit(bytecode.JUMP_IF_FALSE, 0)
        jmp_pos = len(ctx.data) - 1
        self.body.compile(ctx)
        ctx.emit(bytecode.JUMP_BACKWARD, pos)
        ctx.data[jmp_pos] = chr(len(ctx.data))


class Parser(object):
    def __init__(self, tokens):
        self.tokens = tokens
        self.current_token = self.tokens[0]
        self.pos = 0
        self.errors = Error(tokens)

    def get_next_token(self, current_token):
        if self.tokens.index(current_token) < len(self.tokens) - 1:
            return self.tokens[self.tokens.index(current_token) + 1]

    def get_recent_token(self, current_token):
        if self.tokens.index(current_token) > 0:
            return self.tokens[self.tokens.index(current_token) - 1]
        else:
            return current_token

    def parse(self):
        ast = self.main()
        return ast

    def main(self):
        stmts = self.collect_stmts()
        return Block(stmts)

    def collect_stmts(self):
        stmts = []

        brace_count = 0
        opened_brace = None
        x = 0
        while self.current_token.type != EOF:
            if self.current_token.type == RBRACE:
                self.current_token = self.get_next_token(self.current_token)
                return stmts
            if x == 1:
                self.current_token = self.get_next_token(self.current_token)
            else:
                x = 1
            if self.current_token.type == LBRACE:
                opened_brace = self.current_token
                brace_count += 1

            if self.current_token.type != RBRACE and self.current_token.type != LBRACE and self.current_token.type != EOL and self.current_token.type != EOF:
                stmts.append(self.visit_statement(self.current_token))
            # error
            if self.current_token.type == EOF and brace_count > 0:
                self.errors.error(opened_brace)

        return stmts

    def visit_statement(self, token):
        current_token = token
        next_token = self.get_next_token(current_token)

        while True:
            token = next_token

            if token.type == LBRACE or token.type == EOL:
                next_token = self.get_next_token(token)
            else:
                break

        if current_token.type == VARIABLE:
            if next_token.type == ASSIGNMENT:
                right_token = self.get_next_token(next_token)
                if right_token.type == VARIABLE or right_token.type == NUMBER:
                    left_token = current_token
                    return Assignment(left_token.value, self.expr(right_token))
                else:
                    self.errors.error(next_token)
            if next_token.type == BINOP:
                return Stmt(self.expr(next_token))
            if next_token.type != ASSIGNMENT and next_token.type != BINOP:
                return Stmt(self.expr(current_token))

        if current_token.type == NUMBER:
            if next_token.type == BINOP:
                return Stmt(self.expr(next_token))
            else:
                return Stmt(self.expr(current_token))

        if current_token.type == IF:
            cond_token = self.get_next_token(self.get_next_token(current_token))
            if cond_token.type != COMPARISON:
                cond_token = self.get_next_token(current_token)
            cond = self.expr(cond_token)
            # error
            if self.current_token.type != LBRACE:
                self.errors.error(self.get_recent_token(self.current_token))
            stmts = self.collect_stmts()
            return If(cond, Block(stmts))

        if current_token.type == PRINT:
            print_what = self.get_next_token(current_token)
            return Print(self.expr(print_what))

        if current_token.type == LOOP:
            cond_token = self.get_next_token(self.get_next_token(current_token))
            if cond_token.type != COMPARISON:
                cond_token = self.get_next_token(current_token)
            cond = self.expr(cond_token)
            # error
            if self.current_token.type != LBRACE:
                self.errors.error(self.get_recent_token(self.current_token))
            stmts = self.collect_stmts()
            return Loop(cond, Block(stmts))

    def expr(self, token):
        next_token = self.get_next_token(token)
        self.current_token = self.get_next_token(token)
        if token.type == COMPARISON:
            right_token = next_token
            left_token = self.get_recent_token(token)
            return Comparison(token.value, self.expr(left_token), self.expr(right_token))
        if token.type == BINOP:
            right_token = next_token
            left_token = self.get_recent_token(token)
            return BinOp(token.value, self.expr(left_token), self.expr(right_token))
        if token.type == VARIABLE or token.type == NUMBER:
            return self.atom(token)
        # error
        else:
            self.errors.error(token)

    def atom(self, token):
        if token.type == NUMBER:
            return ConstantInt(token.value)
        if token.type == VARIABLE:
            return Variable(token.value)
        raise NotImplementedError


def parse(source):
    lex = Lexer(source)
    parser = lex.create_parser()
    return parser.parse()
